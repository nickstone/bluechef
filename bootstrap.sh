#! /bin/bash

###############################################
### Variables                               ###
###############################################
#Trouble-shoot
    updates_on=0

#Index Clustering
    idxcluster_on=1
    cluster_secret="DoesTh1sNe3d2BCl3artxt"
    cluster_label="ClusterLuck"
    cluster_port="8080"
    cluster_rf="2"
    cluster_sf="2"

#Search Head Clustering

#Read the CLI arguments
if [ $# -eq 0 ] #If no arguments, use defaults
  then
    spk_version=7.2.1
    spk_build=be11b2c46e23
  else
    #Split out arguments sent via cli
    spk_version=$1
    spk_build=$2
    #grab the IDX-Master Array and split into variables
    idxmaster_arg=$3
    idxmaster=(${idxmaster_arg//;/ })
    echo ${idxmaster[@]}
    idxmaster_hostname=${idxmaster[0]}
    idxmaster_ipv4=${idxmaster[1]}
    #grab the IDX-Peers Array and split into variables
    idxpeers_arg=$4
    echo $idxpeers_arg
    idxpeers_array=(${idxpeers_arg//;/ })
fi

#splunk-7.3.0-657388c7a488-linux-2.6-x86_64
#splunk-7.2.6-c0bf0f679ce9-Linux-x86_64
#splunk-7.2.5.1-962d9a8e1586-Linux-x86_64.tgz
#splunk-7.2.5-088f49762779-Linux-x86_64.tgz
#splunk-7.2.4.2-fb30470262e3-Linux-x86_64.tgz
#splunk-7.2.4-8a94541dcfac-linux-2.6-x86_64.rpm
#splunk-7.2.3-06d57c595b80-linux-2.6-x86_64.rpm
#splunk-7.2.2-48f4f251be37-linux-2.6-x86_64.rpm
#7.2.1-be11b2c46e23
#splunk-7.2.0-8c86330ac18-linux-2.6-x86_64.rpm
#splunk-7.1.4-5a7a840afcb3-linux-2.6-x86_64.rpm
#splunk-7.1.3-51d9cac7b837-linux-2.6-x86_64.rpm
#splunk-7.1.2-a0c72a66db66-Linux-x86_64.tgz
#splunk-7.1.1-8f0ead9ec3db-linux-2.6-x86_64.rpm
#splunk-7.1.0-2e75b3406c5b-linux-2.6-x86_64.rpm
#splunk-7.0.7-b803471b1c68-linux-2.6-x86_64.rpm
#splunk-7.0.6-3e6d6611992a-linux-2.6-x86_64.rpm
#splunk-7.0.5-23d8175df399-linux-2.6-x86_64.rpm
#splunk-7.0.4-68ba48f99743-linux-2.6-x86_64.rpm
#splunk-7.0.3-fa31da744b51-linux-2.6-x86_64.rpm
#splunk-7.0.2-03bbabbd5c0f-linux-2.6-x86_64.rpm
#splunk-7.0.1-2b5b15c4ee89-linux-2.6-x86_64.rpm
#splunk-7.0.0-c8a78efdd40f-linux-2.6-x86_64.rpm
#splunk-6.6.11-a4e9ea700cba-linux-2.6-x86_64.rpm
#splunk-6.6.10-2b5f6c3d5f96-linux-2.6-x86_64.rpm
#splunk-6.6.9-7ca2e86659b7-linux-2.6-x86_64.rpm
#splunk-6.6.8-6c27a8439c1e-Linux-x86_64.tgz
#splunk-6.6.7-429660948eb8-linux-2.6-x86_64.rpm
#splunk-6.6.6-ff5e72edc7c4-linux-2.6-x86_64.rpm
#splunk-6.6.5-b119a2a8b0ad-linux-2.6-x86_64.rpm
#splunk-6.6.4-00895e76d346-linux-2.6-x86_64.rpm
#splunk-6.6.3-e21ee54bc796-linux-2.6-x86_64.rpm
#splunk-6.6.2-4b804538c686-linux-2.6-x86_64.rpm
#splunk-6.6.1-aeae3fe0c5af-linux-2.6-x86_64.rpm
#splunk-6.6.0-1c4f3bbe1aea-linux-2.6-x86_64.rpm
#splunk-6.5.9-eb980bc2467e-linux-2.6-x86_64.rpm
#splunk-6.5.8-96271d9ba09a-linux-2.6-x86_64.rpm
#splunk-6.5.7-f44cfc17f820-linux-2.6-x86_64.rpm
#splunk-6.5.6-44f873cfa227-linux-2.6-x86_64.rpm
#splunk-6.5.5-586c3ec08cfb-linux-2.6-x86_64.rpm
#splunk-6.5.4-adb84211dd7c-linux-2.6-x86_64.rpm
#splunk-6.5.3-36937ad027d4-linux-2.6-x86_64.rpm
#splunk-6.5.2-67571ef4b87d-linux-2.6-x86_64.rpm
#splunk-6.5.1-f74036626f0c-linux-2.6-x86_64.rpm
#splunk-6.5.0-59c8927def0f-linux-2.6-x86_64.rpm

create_pwordhash(){
    #Hey Chicken, I'm egg...
  echo "$1 $FUNCNAME"
  clearpassword="hot-bucket"
  cryptopassword="$(/opt/splunk/bin/splunk hash-passwd $clearpassword)"
  echo $cryptopassword
}

restart_splunk(){
  echo "$1 $FUNCNAME"
  echo "Restarting Splunk"
  /opt/splunk/bin/splunk restart
}

create_password(){
  echo "$1 $FUNCNAME"
  ### Need to fix the Chicken/Egg scenario...
  #Preseed the auth credentials
  #http://docs.splunk.com/Documentation/Splunk/7.2.1/Admin/User-seedconf
  touch /opt/splunk/etc/system/local/user-seed.conf
  echo "[user_info]
  USERNAME = admin
  PASSWORD = hot-bucket
  #HASHED_PASSWORD = ${cryptopassword}
  " >> /opt/splunk/etc/system/local/user-seed.conf
  #PASSWORD = hot-bucket
  #HASHED_PASSWORD = $6$YLsuIzCGep3Uflvv$svPCkcyHNAcBQRq/ZqX9xEQYp4fBxiTdxhvUSR/G2MWFgjHuvbd28eMIvFzArBfEfwKWOMOK81znmcursCbjc0
  }

license_splunk(){
  echo "$1 $FUNCNAME"
  if [[ "$HOSTNAME" =~ spksh0(1|2) ]];
    then
      #Install ENT license
      echo "Installing Current Lab NFR License"
      mkdir -p /opt/splunk/etc/licenses/enterprise
      #cp /licenses/$SPLUNK_LIC.lic /opt/splunk/etc/licenses/enterprise/splunk_license.lic

      /opt/splunk/bin/splunk edit licenser-localslave -master_uri self -auth admin:hot-bucket
    else
      echo "Joining existing License Master"
      /opt/splunk/bin/splunk edit licenser-localslave -master_uri https://spklic.test-lab.local:8089 -auth admin:hot-bucket
    fi
}

#Check Linux Platform in use
spk_platform=linux

check_os-platform(){
  echo "$1 $FUNCNAME"
  linux_ver="$(uname -a)"
  echo "$linux_ver"

  if [[ $linux_ver =~ Linux.*Ubuntu.* ]];
    then
      echo "You are installing on Linux - Ubuntu"
      if [ $updates_on -eq 1 ]
        then
          apt-get update
          apt-get install -y jq whois build-essential git unzip tcpdump
      else
        echo "Skipping updates and installing required packages only"
        apt-get install -y jq whois build-essential git unzip tcpdump
      fi

    #RPM uses x86_64 and DEB uses amd64
      spk_packtype=deb
      spk_arch=amd64
      spk_filename='splunk-'$spk_version'-'$spk_build'-linux-2.6-'$spk_arch'.'$spk_packtype

  elif [[ $linux_ver =~ Linux.*el7.* ]];
    then
      if [ $updates_on -eq 1 ]
        then
          yum -y update
          #Check why these are being install, I think some are legacy from my forked script
          yum install -y whois git unzip make automake gcc gcc-c++ kernel-devel bind-utils tcpdump
        else
          echo "Skipping updates and installing required packages only"
          yum install -y whois git unzip make automake gcc gcc-c++ kernel-devel bind-utils tcpdump
        fi

      spk_packtype=rpm
      spk_arch=x86_64
      spk_filename='splunk-'$spk_version'-'$spk_build'-linux-2.6-'$spk_arch'.'$spk_packtype

  elif [[ $linux_ver =~ .*amzn.* ]];
    then
      if [ $updates_on -eq 1 ]
        then
          yum -y update
          #Check why these are being install, I think some are legacy from my forked script
          yum install -y tcpdump whois git unzip make automake gcc gcc-c++ kernel-devel bind-utils
        else
          echo "Skipping updates and installing required packages only"
          yum install -y tcpdump whois git unzip make automake gcc gcc-c++ kernel-devel bind-utils
        fi

      spk_packtype=rpm
      spk_arch=x86_64
      spk_filename='splunk-'$spk_version'-'$spk_build'-linux-2.6-'$spk_arch'.'$spk_packtype
  else
      echo "Do you even Linux?"
  fi
}

download_syslog-ng(){
  echo "$1 $FUNCNAME"
  #Install non-yum syslog-ng
  wget --progress=bar:force https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
  rpm -Uvh epel-release-latest-7.noarch.rpm
  sudo rpm -Uvh epel-release-latest-7.noarch.rpm
  cd /etc/yum.repos.d/
  wget --progress=bar:force https://copr.fedorainfracloud.org/coprs/czanik/syslog-ng39/repo/epel-7/czanik-syslog-ng39-epel-7.repo
  yum install -y syslog-ng
  }

install_splunk(){
    echo "$1 $FUNCNAME"
    echo "Installing Splunk!"

    #I wonder if I could consolidate this into only vars using the abov logic
    #deb vs dpkg would be the first problem
    if [ $spk_packtype = "rpm" ];
      then
      echo "$spk_filename"
      rpm -i $spk_filename

    elif [ $spk_packtype = "deb" ];
      then
      echo "$spk_filename"
      dpkg -i $spk_filename

    else
      echo "bugger!"
    fi
  }

  enable_ldap-auth(){
  echo "$1 $FUNCNAME"
    #Enable LDAP authentication for Splunk
  echo "Enable LDAP authentication for Splunk"
  cp /vagrant/authentication.conf /opt/splunk/etc/system/local/
  }

start_splunk(){
  echo "$1 $FUNCNAME"
  #Start Splunk
  # should I add the --seed-passwd switch here?
  # https://docs.splunk.com/Documentation/Splunk/7.2.1/Security/Secureyouradminaccount#Create_admin_credentials_for_automated_installations_with_the_.27hash-passwd.27_CLI_command
  /opt/splunk/bin/splunk start --accept-license --no-prompt --answer-yes
}

stop_splunk(){
  echo "$1 $FUNCNAME"
  /opt/splunk/bin/splunk stop
}

reset_kvstore-perms(){
  echo "$1 $FUNCNAME"
  #Reset the KVStore Key permissions
  chmod -R 400 /opt/splunk/var/lib/splunk/kvstore/mongo/splunk.key
  rm -rf /opt/splunk/var/lib/splunk/kvstore/mongo/mongod.lock
}

centralise_secret(){
  echo "$1 $FUNCNAME"
  #Change splunk.secret file
  echo "Changing splunk.secret File"
  cp /vagrant/splunk.secret /opt/splunk/etc/auth/
}

configure_idx-replication(){
  echo "$1 $FUNCNAME"
  echo "Identify Cluster Master from Peer nodes"
  #if [ "$HOSTNAME" = "cluster-master" ]
  echo $idxmaster_hostname
  if [[ "$HOSTNAME" == "$idxmaster_hostname" ]]
  #Enable the Cluster Master
    then echo "Creating Splunk Index Cluster Master"
    /opt/splunk/bin/splunk edit cluster-config -mode master -replication_factor $cluster_rf -search_factor $cluster_sf -secret $cluster_secret -cluster_label $cluster_label -auth admin:hot-bucket

#    #Create new indexes (this should really be controlled by the TA's)
    echo "Creating Cluster Index bundle configurations"
    touch /opt/splunk/etc/master-apps/_cluster/local/indexes.conf
    echo "[test]
    repFactor = auto
    homePath = \$SPLUNK_DB/test/db
    coldPath = \$SPLUNK_DB/test/colddb
    thawedPath = \$SPLUNK_DB/test/thaweddb" >> /opt/splunk/etc/master-apps/_cluster/local/indexes.conf
  #/opt/splunk/bin/splunk add index wineventlog -auth 'admin:changeme'
  #/opt/splunk/bin/splunk add index osquery -auth 'admin:changeme'
  #/opt/splunk/bin/splunk add index osquery-status -auth 'admin:changeme'
  #/opt/splunk/bin/splunk add index sysmon -auth 'admin:changeme'
  #/opt/splunk/bin/splunk add index powershell -auth 'admin:changeme'

  elif [[ "$HOSTNAME" =~ spkidx0(1|2) ]]
    then
      #Enable the Peer Node
      echo "Creating Splunk Index Cluster Peer"
      /opt/splunk/bin/splunk edit cluster-config -mode slave -master_uri https://$idxmaster_ipv4:8089 -replication_port $cluster_port -secret $cluster_secret -auth admin:hot-bucket

  else
    echo "Not sure what you are..."
  fi
}

configure_input(){
  echo "$1 $FUNCNAME"
  # Add a Splunk TCP input on port 9997
    echo "Adding TCP input port 9997"
    /opt/splunk/bin/splunk enable listen 9997
    #mkdir -P /opt/splunk/etc/apps/search/local
    #touch /opt/splunk/etc/apps/search/local/inputs.conf

    #Force connection based on IP address, not on DNS name
    echo -e "[splunktcp://9997]
    connection_host = ip" > /opt/splunk/etc/apps/search/local/inputs.conf
  }

configure_webconf(){
  echo "$1 $FUNCNAME"
  # Enable SSL Login for Splunk
  echo "Enable WebUI TLS"
  echo "[settings]
    enableSplunkWebSSL = true
    #Don't check for new version
    updateCheckerBaseURL = 0
    #Change port to port 443. Makes firewall rules easy and outbound proxy goes away.
    httpport = 443
    # Force TLS 1.2 (Should be default)
    sslVersions = tls1.2
    " > /opt/splunk/etc/system/local/web.conf
}

configure_uiprefs(){
  echo "$1 $FUNCNAME"
  # Modify base UI Preferences
  echo "Change base search time to 4 hours"
  echo "[search]
    dispatch.earliest_time = -4hr
    dispatch.latest_time = now
    " > /opt/splunk/etc/system/local/ui-prefs.conf
}

configure_searchpeers(){
  echo "$1 $FUNCNAME"
#Add Search configuredPeers
if [[ "$HOSTNAME" =~ spksh0(1|2) ]];
  then
    for element in "${idxpeers_array[@]}";
    do
       subarray=(${element//,/ })
       idxpeer_ipv4=${subarray[2]}
       echo $idxpeer_ipv4

       /opt/splunk/bin/splunk add search-server https://$idxpeer_ipv4:8089 -auth admin:hot-bucket -remoteUsername admin -remotePassword hot-bucket

    done;
  else
    echo "You get no search peers, you're not a search head"
  fi
}

start_onboot(){
  echo "$1 $FUNCNAME"
  # Reboot Splunk to make changes take effect
  echo "Enable Splunk to start at boot time"
  /opt/splunk/bin/splunk enable boot-start

  # Skip Splunk Tour and Change Password Dialog
  touch /opt/splunk/etc/.ui_login

  echo "Updating Splunk boot script for ulimits 8192 hard and soft"
  sed -i '/init.d\/functions/a ulimit -Sn 8192' /etc/init.d/splunk
  sed -i '/init.d\/functions/a ulimit -Hn 8192' /etc/init.d/splunk
}
check_os-platform

#remove firewalld
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo systemctl mask firewalld

#install iptables
sudo yum install -y iptables-services
sudo systemctl start iptables; sudo systemctl start ip6tables
sudo systemctl enable iptables

download_syslog-ng

#Download Splunk
if [ -f "/opt/splunk/bin/splunk" ]
  then echo "Splunk is already installed"
else
  #Check to see if we have a local copy in the /vagrant share
  if [ -f "/vagrant/binary/"$spk_filename ]
    then echo "Copying shared copy of Splunk"
    cp /vagrant/binary/$spk_filename /root
  #Check to see if we have a copy in the current directory
  elif [ -f $spk_filename ]
    then echo "Splunk is already downloaded!"
  else
    echo "We need to download Splunk. Downloading now..."
    # Get Splunk.com into the DNS cache. Sometimes resolution randomly fails during wget below
    dig @8.8.8.8 splunk.com
    # Download Splunk
    wget --progress=bar:force -O $spk_filename 'https://splunk.com/page/download_track?file='$spk_version'/'$spk_platform'/'$spk_filename'&ac=&wget=true&name=wget&platform='$spk_platform'&architecture=x86_64&version='$spk_version'&product=splunk&typed=release'
  fi

  install_splunk

  centralise_secret

  #enable_ldap-auth

  #create_pwordhash
  #echo $cryptopassword

  create_password

  start_splunk

  reset_kvstore-perms

  license_splunk

if [ $idxcluster_on -eq 1 ]
  then configure_idx-replication
else
  echo "Clustering has not been enbaled in the variables configuration"
fi
  #Copy apps to Splunk (Change this to copy to Deployment Server)
  #/opt/splunk/bin/splunk install app /vagrant/resources/splunk_forwarder/splunk-add-on-for-microsoft-windows_483.tgz -auth 'admin:changeme'
  #/opt/splunk/bin/splunk install app /vagrant/resources/splunk_server/add-on-for-microsoft-sysmon_607.tgz -auth 'admin:changeme'

  configure_input

  fi

  # Add props.conf and transforms.conf
  #cp /vagrant/resources/splunk_server/props.conf /opt/splunk/etc/apps/search/local/
  #cp /vagrant/resources/splunk_server/transforms.conf /opt/splunk/etc/apps/search/local/

  configure_webconf

  configure_uiprefs

  configure_searchpeers

  start_onboot

  restart_splunk


#Is this host a deployment client?
#/opt/splunk/bin/splunk set deploy-poll $DEPLOY_SERVER --accept-license --answer-yes --auto-ports --no-prompt -auth admin:changeme
#or will it be deployment server?

#Is this host License Master or Slave?
#splunk edit licenser-localslave -master_uri https://licensemaster:8089
#	./splunk edit licenser-pools foo -description test -quota 10mb -slaves guid1,guid2
#	./splunk edit licenser-pools foo -description test -quota 10mb -slaves guid1,guid2 -append_slaves true
#	./splunk edit licenser-localslave -master_uri https://myhost:8089
#	./splunk edit licenser-localslave -master_uri self
#	./splunk edit licenser-groups Foo -is_active 1
